---
layout: post
title:  "Remembrances of a Tomato Garden"
date:   2018-12-24
categories: poems
---
My childhood friend and I are hauling  
my brother’s old aquarium out of the basement  
sneaking up the backstairs,  
 “nature Girl” she mocks affectionately  
The two of us  laughing and giggling.  
After all, I don’t want him to get any ideas, I don’t want any flack.  
It’s Spring and time for me to pull out the trowel,  
And  create my miniature glass world  
of fern fiddleheads, violets, verdant green carpets of velvet mosses,  
My springtime Terrarium!  
  
  
Do you remember the photograph you took of me long ago  
standing in my father’s garden  
I am holding his shovel spoofing “American Gothic”  
Behind me a yard laid bare and  fallow without his handiwork.  
He had died suddenly the year before,  
I can see the dirt on my hands in the photograph.  
wanting to reach him this way, but discouraged  
Everything is now second rate and shoddy, no rhubarb,  
the raspberries are dwindling and no tomatoes, not a single one.  
  
  
I remember the seasonal rituals and duties of this place,  
His tomato garden, year after year, summer after summer  
And accompanying it, the omnipresent compost pile,  
filled to the brim with leaves, and  
all year long we’d add to this concoction  
Kitchen scraps, egg shells, coffee grounds,    
bursting out of the wire fence in late November,  
it held such importance for his plantings.  
So many endless leaves to rake: maples, chestnuts and oaks.  
My brothers shouting, laughing, metal rakes scraping, scraping  
Laying sheets and painting tarps like large ghosts in the big yard  
dusty leaves piled high and their sweet pungent smells,  
marking so many autumns of my childhood.  
  
  
Then in early spring, maybe that first night of warmth  
Forsythias, first cherry’s emerging like  
tiny little lanterns of yellows and pinks glowing In the night.  
following him in the yard, with a skip in my step like young girls would do –  
eager to see what the long winter had produced:  
oily brown, wet, squished-down, decomposed, flattened leaves.  
I hear his eager voice, lighter and cheery  
All the stress and strain removed.  
He’s free of himself for a little while-  
planning, drawing pictures in his mind,  
plotting out on graph paper where the tomatoes will be,  
and maybe he’ll try lettuce this year.  
As my father turned the clumps and leaves over and over with his shovel,  
a kind of mist smoked out of the immense pile.  
I marvelled at this mysterious potion magically transformed over the winter.  
  
  
Navigating through thorny scraggly vines in late summer.  
My sister and I racing out before dinnertime,  
Pulling the good ones, bursting deep orange, right off the vine  
gathering in the fall, wrapping the unripened green ones in newspaper,  
And, to my mother’s exasperation -  still so much yield!  
She stepped aside, while we  worked the kitchen like a factory.  
the old stainless steel counters a sea of oranges and reds,  
my father and I  canning relish and stewed tomatoes by the hour.  
his dedication to these marvelous red fruits and me working  
alongside him, the industrious thirteen year old daughter  
Pots, boiling water and the meticulous timing,  
and how it made me nervous sometimes.  
But I remember this  now in the writing  
The humble beauty of the mason jars lined up  
the  pride we felt in the production line we had created  
And the companionship we had on those days.  
  
  
Leafing through old photos today I found that one -  
my “American Gothic” parody.  
And I went on that long journey back to that ancient memory  
And the stories I told you when a big family lived  
In a  grand old stone house, they had a yard with a lovely garden  
and a father taught a young daughter  
To  pick up a shovel, dig in the dirt  and love the gifts of nature.  
Loving it for face value,  
and knowing the returning gifts at the end of the day  
 or the end of the long season.  
  
  
Today I’ll divide up the Hostas, and replant the Iris,  
clean out the beds  
It seems as though I’ve done this forever,  
but each Spring it feels new again  
 like he is teaching me for the first time,  
Hands in the dirt, I’ll try to reach him again,  
 And He’ll follow me out to the garden  
pressing me on to work a little bit harder  
“go for the wicket “he’ll say  
I’ll unearth something new in him, something not known before  
And I’ll talk a blue streak about you.  
here in my garden, in my imaginings,  
or at least on this page for a little while.  
