---
layout: post
title:  "Attic Visitor"
date:   2018-12-26
categories: poems
---
You fell out of a box in the attic the other day.  
Like a bird caught in the rafters, visiting me in the dark for a little while.  
The yellowed photograph from the beginning of the the last century  
Fluttered to the floor. Girls High, class of 1914  
“Cutest  in the Class”  written in  ancient public school girls calligraphy.  
You were the littlest one in the front row center  
with that oversized Edwardian bow.  
Staying with me all that day, following me, correcting my posture,  
Curling my hair,  and you’re so out of breath in my memory,  
Climbing the stairs, pausing step by step, with that one lung,  
Correcting the siamese cat, his paws catching your stockings  
peeling the potatoes, laying the chunks of cheese on the macaroni.  
  
You were the survivor twin,  so small they carried you around on a tiny pillow.  
Living in a generation of so many losses and you lost your father too.  
So young you barely knew him.  
At the beginning of a new century he was found on a street corner unconscious  
taken away to a holding cell, no chance for a hospital  
For an Irish Catholic in Philadelphia back then.  
Your mother wringing her hands in the kitchen  
waiting for the trolley to deliver him home to Mantua street.  
Independence day flags unfurled and waving  
But your father never came.  
Dying in that prison cell before your mother ever knew  
What had become of him.  
  
We circled around your hospital bed that Bicentennial weekend.  
A low recitation of the rosary  
And your obsession over laying out our Father’s blue cereal bowl  
always in the service to all of us,  you had the grace of giving  
And I remember being so scared, that much I remember  
But you can’t know what it is when you are nineteen  
Passing by your bedroom,  
The blue mirror crucifix throwing lights, tiny prisms  
On the wall like unanswered prayers  
wanting to see you there  
The afternoon sunlight and the shadows looked so different,  
And your absence in our house, was like a dark syrup  
Seeping into my brain, everything felt closed in  
And I lost my nerve and  all of  my hope that summer.  
  
But today you are here to tell me something,  
Whispering gently in my ear, my head is on your lap, tired little girl  
Recovering from the Ocean City Boardwalk  
Sitting next to you in the backseat of that Black Chevrolet Station Wagon  
You’re laying your hands on my heavy head  
The doppler sounds of the cars rush by  
I feel your comfort and your quiet affection  
  
I don’t know if I believe in a God of eternal salvation  
But I do believe in the here and now and a kind of  
chain of memory and connection to them, those who brought us here safely,  
They re enter our world and visit us in our daily  musings.  
In the kitchen, climbing the stairs, making the bed.  
And rummaging through the attic brought you back to me,  
My belief that I am talking to you again, and seeing you  
Feeling you nudging my back, “Sit up straight Maryann”,  
It’s as  real as the interminable crow  
complaining this May night outside my window.  
You’re telling me how to handle the tough parts.  
Now I know why you worried so much.  
It’s simply love, you say,  
it’s simply love.  
