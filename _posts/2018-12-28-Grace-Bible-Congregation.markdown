---
layout: post
title:  "Grace Bible Congregation"
date:   2018-12-28
categories: poems
---
Today I meditate on the mallard duck  
that crossed Sandy Run creek one morning  
he and his entire family crossing paths with me  
while running over the ancient bridge,  
The delight I felt that Spring morning in seeing them,  
His bottle green feathers against the brown, starched white collar  
And the shimmering sky blue at the back of his wings  
And accompanying him his unadorned wife and the timid ducklings.  
I stopped breathless and leaned on the railing,  
It’s a good reliable image for mindfulness  
I listen to my breath trying to  create a rhythm.  
fidgeting at first then staring into the fireplace flames,  
Smiling in seeing the profiles of Washington and Jefferson  
in  the  fireplace logs,  
Trying to empty a mind of worry, thoughts backtrack to yesterday and the people  
of Grace Bible Congregation,  
  
They touched us in a  way we couldn’t explain,  
If I try, I feel tears rising , and a burning in the back of my throat  
and embarrassment in the telling to a secular world, an agnostic son,  
we are all smart skeptics,  not believing in a world  
coming together in six days,  
Or an omniscient master planner.  
No,  it wasn’t that ,  
it was the brightness of their faces, their devotion,  
Their attention to each other and to  
to The Big Book - the Word of God.  
And cheerful confident sixteen year old Mary  
with the long braid, one single plait  
falling  far past her shoulders  
Reading with conviction from her dog-eared Bible  
laughing at my self-deprecating fallen-away Catholicism  
“Start with Romans” she said.  
  
Asking their names, It was about them  
the teenagers, we were drawn to them and  
they were a throwback, wondering why  
Not one of them had an isolated look, nor tethered to cell phone.  
We had a small conversation with them but  
My thoughts lingered over them for hours  
And I felt a love for them  
Flannel shirt, tall, thin and unassuming  Bill  
And pretty Amy welling up tearful  
“We could be fast friends “ I assure her  
Me asking questions about Jesus  
“I believe he was an extraordinary man, “  
And Amy shy but resolute answering without bluster  
“we believe he was God and this book is the truth of all he spoke”  
And Bill, this quiet steadfast leader,  
Of  kindly believers adopting the doubters.  
and all of us joined in a circle of prayer,  
Holding hands in a Quizno in Washington DC  
Bill’s quiet voice praying for us  
“God Bless Maryann and John as they travel.  
In your mercy Lord, just bless them.”  
  
The Mallard Duck came back to to me for a moment  
Slowly considering his path along the spring creek.  
Then I hear my mother’s voice, I am so small,  
She is holding my hand tightly,  
in the department store of my childhood,  
my sweet and optimistic  mother  
Bosomy petite, in a shirtwaist dress, and a Camel Hair coat,  
striking up a conversation with the lady In Gimbels,  
“The Easter hats are out so early this year! “  
Running my tiny fingers over the satin ribbons and straw brims.  
And they talk as if they know each other.  
But I am four years old  and  
impatient to order chocolate shakes and rice pudding  
And be hoisted up to the revolving cakes and pies.  
My sister and I throw pennies and make wishes in the fountain.  
  
The memory and the meditation is broken  
By the front door slamming,  
and you walking towards the car,  
You are hesitant, your steps are not spry or deliberate,  
They are weighted and you pause  
Staring down the street, the easy swoop of a hawk  
Overhead. You are off to bring your sickly old mother back to us.  
I looked out at you from above in the second floor window,  
a grey midwinter afternoon and you’re discouraged sad eyes,  
I see your feelings clear through this window,  
Then out to you on the sidewalk, a gust of a winter wind  
Catches you unawares and I tap on the window, waving.  
And I feel them - the congregation, they want to console you  
the Grace Bible believers,  
and smiling enthusiastic Mary is laughing  
Proudly pulling out her dog-eared Bible.  
The long plaited hair framing her pretty face  
And in that moment all of my cynicism  
Is replaced with something pure  
voices hushed and waiting,  
We are waiting for humble Bill to start,  
and the beautiful young people, anachronistic and  
praying to a God we want to believe in,  
And we can for a little while  
gathering around you  
their young eyes follow Amy’s direction  
And just above a whisper  
the quiet power of Bill’s voice  
“God Bless John  
like the lilies in the field,  
Take away his worry Lord  
In your mercy, just bless him Lord  
bless him Lord.”  
