---
layout: post
title:  "June Morning"
date:   2018-12-31
categories: poems
---
Everything would have changed that day  
If I had turned my head from the highway,  
Just for that split second, to the bright farm and the silo  
On the other side of the road,  
My eyes always drawn to that,  
Driven back to Andover  
To those old Irish families,  
Early 1900’s weathered and sore  
From bringing  up the potatoes.  
Maybe a reverie of my grandfather dressing and  
Scrubbing the dirt from his young man’s hands,  
Placing his finest serge suit on the bed,  
His mother calling  up to him  
“Will, the ferry leaves in fifteen minutes for the social in Clyde”  
He would go with his cousin Mary O’Leary  
And he would meet Hazel there,  
A pretty school teacher  
With a bellowing laugh and long Auburn hair.  
  
Everything would have changed that day  
And I’d no longer feel the sun on my aging face,  
And my body fifty-nine strong,  
And my feet pounding-pounding out  
A steady rhythm, a long ribbon  
Of time and memory.  
Climbing up Twining road to the antique graveyard.  
And you back at home fixing something,  
A bike, an invention, a contraption,  
Nuts and bolts and a level and  
A mind clear, in its freedom on a fine morning.  
Waiting for me to return  
Where soon you’d meet me in the yard,  
We’d talk about the boys and  
Marvel at the knockout roses, and the winterberry tree.  
The new sun shining on us  
And spilling onto the young neighbor boy  
Darting out into the new morning.  
  
Everything would have changed that day.  
A phone call and a rookie policeman’s voice green and unsteady,  
Somewhere on the side of the road  
On that clear blue June morning.  
