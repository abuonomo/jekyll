---  
layout: post  
title:  "The Basement Theatre"  
date:   2018-01-14  
categories: essays  
---  
My clever father had created a stage in our basement.  He picked up windows from old scrap yards, or houses that had gone to the wrecking ball. And then he built the windows into the walls of our old basement. Behind the windows he would place a poster of a scene, glued to the plaster walls, so that it looked like a real place.  My father was a fan of Westerns, and he fashioned this stage to feel like those old movies of his youth. There were saloon swinging doors, and a mock half roof with shingles on the top, and that stage with boards, ran all around our immense basement.  I loved being on that homemade stage. There was this very large room with the stage, and the room beside had a ping pong table and a pool table. A deep burgundy velvet cushion  ran the length of the wall on a large bench.  And there my father had placed another poster, a sunset or a cityscape, behind two multi pane windows. Thinking of my Dad on a Saturday laying those boards just right, my dear Grandfather helping him.  I don’t remember that underground paradise any other way.. So it must have been built when I was very little. The whole neighborhood embraced this magical place, primitive in its own right, but a place where your creative spirits could soar.  
  
I’d stretch out on the lengthy regal velvet cushion watching my brothers and sister play pool. I participated too, and, oh I’d go through the motions. There was that coolness of playing pool - chalking the cue, leaning in to the brilliant green felt top, crouching down, closing one eye, talking it up, hearing the satisfying  crack of the balls, and for me... watching it drift, to some unremarkable place.   On the other hand,  my younger sister had inherited my older brothers’ skills - that special hand eye coordination, that flick of the wrist, that planned out shot. There was a fourth grade nineteen sixties hipness to my sister.  Her hair was a medium dark blonde way down her back, in the style of the day. And she was able to have the appropriate straight bangs, just above her eyes. And with her great luck, and to my Grandmother’s chagrin, her hair never held the Shirley Temple ringlets my Grandmother had labored over the night before.  I, on the other hand, had a hair style appropriate to the nineteen fifties,…. side parted on my broad forehead, and ringlets steadfast and unshakeable, awkward and anachronistic.  
  
I wore orthopedic shoes all through grade school because of severe intoeing of my right leg and foot. All the other girls were in Penny Loafers or pointed Mary Jane’s.  Oh yes, I was jealous of them, but, thinking back, I had an affection for those red oxford shoes. I can hear my twelve year old feet clomping in them, worn out  laces, and scuffs galore, and the sound gave me a kind of power, I was a theatre girl and an actress. My place was on that homemade stage my father had built.  
  
Twenty five cents a person, on Saturday afternoons , we’d put on shows. my sister, brothers and I. We were the younger four of a family of seven children, often referred to by my parents as “The Little Ones.”  Gathering the kids in the neighborhood and our loyal grandparents, lining up makeshift chairs, I can still hear all of them, those encouraging heightened voices and hesitant steps of my grandparents, and all the neighborhood kids, bounding down those cellar steps to our basement theatre.  And one year, my sister, my older brothers and I,  performed our father’s original play  -  “The Long Lost Brother. ” Lights turning down, kids swinging legs in chairs half mocking, my elder’s proud whispers, my heart would race as if I was on the Broadway stage. And we children, ended every performance with the same song, every performance, every time - “ Moon River. ”  
  
What is it about that song that is a bridge to this special time and place? It’s a memory of an unbounded childhood of the sixties, and a connection to my father, the eccentric craftsman, toiling with posts and beams Saturday afternoons in the early sixties.  “Moon River” - the Henry Mancini original is the one that connects - those lovely strings, the lonesome harmonica, those corny, old -school choruses so emblematic of the time. Pressing shoulders with my brother, our voices echoing, trying to outdo one another, I catch a glimpse of my white knee socks and those dark red oxford shoes, clunky yet tenacious on my inverted feet.   
<br />
Moon river, wider than a mile  
I'm crossing you in style some day  
Oh, dream maker, you heart breaker  
Wherever you're going, I'm going your way 
{: style="color:black; font-size: 95%; text-align: center;"} 
  
Two drifters, off to see the world  
There's such a lot of world to see  
We're after that same rainbow's end, waiting, round the bend  
My Huckleberry Friend, Moon River and Me  
{: style="color:black; font-size: 95%; text-align: center;"}  
  
  
