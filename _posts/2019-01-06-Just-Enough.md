---  
layout: post  
title:  "Just Enough"  
date:   2019-01-06  
categories: essays  
---  
Walking with a friend on a Spring night,  
laughter and a couple of beers,  
A sudden severing sound, screeching tires, and a voice,  
and the day had no more promise left in it  
Stopped short in front of you was a nightmare needing a fix.  
A man overaged for his bicycle  
“I don’t want any trouble” he mumbles  
  
All you remember is the silver, the black and the hole and  
A kind of dark tunnel you went through, becoming enlarged in front of you  
faces, lives, loves, your girlfriend, your brothers, your mother and father  
Tossing the money and the wallet and shaking terrified,  
you turned and ran, feet slapping the sidewalk  
In a desperate pace you’ve never known  
Passing rowhomes, and a young girl crying on a porch step,  
Friday Nights in Fishtown, young drinkers  
and Bowie’s “Rebel, Rebel” pouring out of the corner bar.  
  
Returning home to your small town, you turn up the winding crescent of your street,  
you know these houses and the rise of the hill by heart  
It’s early spring, but there’s no comfort in it tonight  
The rhododendrons feel garish and out of sync.  
  
Early in the day you sat by the pond with your mother,  
you talked about Von Humboldt the German naturalist  
Darwin and Thoreau inspired by him, you’re an explorer too and you liken yourself to him  
But now you lie in your bed, and your heart is a hammer  
and fear runs through you a kind of roaring river,  
your eyes finally shut and you dream of Humboldt,  
The eighteenth Century man, his arms laden with scientific instruments  
Boxes of shells and plants at his feet  
He is calling out to you, waving you on to join him  
to be his travel companion to mountains, rivers, and a world of natural beauty  
  
You awake to the meek sound of one solitary mourning dove,  
Early sunlight, a golden ribbon on your bed.  
The comfort of the relics in your room,  
the maps, and bottles of sand, books, your music.  
This museum of your young life is  
A kind of balm softening the blows of the night  
You think about your friend and the laughter  
And your surprising empathy for the man, forlorn, sunken eyes gazing  
Just then from your window, a pale thin slice of a morning moon  
and in the distance, the lilt of your mother’s voice, early morning cheerful  
It’s  enough, just enough of the Cosmos  
to bring you back to all that is good  
