---
layout: post
title:  "The Ice Storm"
date:   2019-04-27
categories: poems
---  
At the end of my midwinter dusk run  
the lights are coming on in the houses  
Just enough darkness to travel back  
Not enough light and just enough darkness  
To pretend -  
The cars are depression era gloomy and overgrown.  
I run by your childhood home,  
And there you are in the living room -  
Hair swept back - Black Irish Mayo good looks  
You and your brothers sprawled around the immense radio  
All gangly legs and arms of youth  
The King has just abdicated  
And your mother so moved to tears hearing his voice  
  
Static, lovely and sad stretched out over an ocean.  
Maybe I think there is a certain power  
or prayer in a vision  
My twenty something self lost your fifty something self,  
And I stand here foolish  
stopped dead in my tracks.  
Lifting my arms up reaching you through the light of the windows  
I hear your voice,  
A seventeen year old voice I never knew  
“Save a piece of pie for Daddy”  
“Daddy” said in a dipthong lingual last vestige  
Of your Lake Ontario summers.  
  
I drag my breath through the cold night  
Late February Oaks and Maples line my street  
like looming giants  
their dark finger branches hold up the night’s canvas.  
A sudden wind and drops of ice and hail  
Stabbing my face, unyielding,  
Huffing and puffing as fast as I can over the crest of the hill  
John calls out to me high pitched and worrisome.  
Like a parent - he pulls me in the door of our house,  
I am all tears and breathless as he hugs me saying  
“You must be freezing - you’re covered in ice”  
The crystals and chunks fall from me like a kind of cage  
Breaking on the sidewalk and spilling into the hall.  
  
The sky is an inky black now  
And I know I am not young anymore-  
But face pressed against the glass like a little girl  
I listen to the ice filling the streets  
Like many whispers.  
