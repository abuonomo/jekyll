---
layout: post
title:  "The Missing"
date:   2020-03-28
categories: poems
---
I drive eerie streets today in the land of the missing.  
I miss you disheveled St. Luke’s kids, backpacks, glum looks, missing hats on cold mornings,  
I miss you pretty millennial girl, chronically late, in purple high heels and wild hair running down the hill for the train on Roberts Avenue,  
I miss you sleepy eyed school girl on Mount Carmel Avenue, eating cereal on a stepstool at the curb waiting for the bus,  
I miss our morning wave, and your pretty smile, crossing guard in front of the church, long chocolate hair flowing out of your white cap.  
I miss you rounded oversized red faced woman on the corner of Ridge Avenue,  
Ubiquitous Eagles cap on your head, and the way you're always tilted back, arms up in a laugh  
with your friend.  
  
Tomorrow morning I’ll place you back in the scene of my morning ride,  
a kind of magical thinking I’m good at. Back in the ebb and flow of a workday morning.  
Just like I did on my brothers’ model train platform long ago, late at night,  
sneaking down the basement, crouching down into a miniaturized world, pushing  the buttons,  
the train rounding the track  
It’s  solitary beam of light  kindly putting the town back together,  
line them up, all of them, put them in positions, in front of their houses, on benches, laughing,  
heads turned, waving into the morning to meet my gaze.  
Until I lost you, all of you, to a virus  spread round the world, I never knew I loved you  
never knew I loved you until now.  
