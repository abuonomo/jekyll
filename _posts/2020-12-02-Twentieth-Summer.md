---
layout: post
title:  "Twentieth Summer"
date:   2019-07-27
categories: poems
---
​But I was twenty once, a self deprecating funny girl,  
Here’s my girlfriends, Mary, her hair’s way down her back,  
Skinny pretty hippie,  
there’s Annie, tomboy cute, and earliest girl in flannel  
My brother, sister, and the handsome boy from Holland.  
so unstudied studied back then, no makeup  
don’t brush your hair too much  
we’re wearing boy’s corduroys we bought at the Army Navy store.  
Drinking Miller High Life while the other girls drank white wine Spritzers  
Cars doors slamming, there’s a promise ahead on the road.  
You’ll sit in the back seat with my brother, he’ll have you in tears with laughter  
hopeful first light of a summer night, all neon, and a hundred voices.  
The band’s playing all the “Yes” songs in the background,  
we know all the words, reaching hands, arms swaying, glasses up high,  
Clinking imprints of our fingers on the frosted glasses of the nighttime.  
All of us joined in something so fleeting as a shooting star,  
laughing and shouting to be heard  
But we like it, the yelling is part of the joy  
it’s a time where you’re lit up inside your soul, grabbing the night and  
The edge of summer’s gifts  
One minute it’s bright, then it’s gone. Did you see it?  
We’re toasting and shouting “to the summer of 1977!”  
  
And now the announcer’s, ”last call!”  
fluorescent lights invade our weary joy, It’s sudden, and  
a hundred voices Groan...”awww nooo...!”,  
Closing time, we barrel down Dune Drive in Mary’s 1962 Corvair.  
Unsafe at any speed, But my pretty younger sister’s always the designated,  
but don’t let her preppy collars, sweaters, and loafers deceive you,  
One eye shut she’ll get us home, I know.  
Your sides are splitting, see, it’s just like I told you,  
that’s the way it is with my brothers, that’s the way it is,  
like a great show that you’re part of, and you’re just waiting for the next funniest thing.  
The car seems to float on the seashore road, gales of laughter  
leaving echoes in the night.  
I see your pretty smile, and your doubled over laughing your sweet infectious laugh,  
At my brother’s imitation of the guy that asked me to dance.  
How we dominated the dance floor with our mocking disco moves  
and the people circled around us unison clapping.  
  
*“There’s a time and the time is right and it’s right for me  
It’s right for me and the word is love”​*  
  
Let’s switch places time traveler, lucky girl  
Where I was a young once with all the gifts ahead of me  
Only the gifts, but none of the sadness, and loss, none of the frightening things  
Where the ones you loved with all your might, really die,  
that day, that turned into decades  
Light a match in front of your face and watch it change in the moonlight  
You’ll want them to know you’re still that pretty girl, the mocking affectionate friend  
And the girl that took over the dance floor  
But you can’t fake it, your face is wrinkled and  
They’re ignoring you, bumping into you, you’re like a ghost in a theme park, Unable to ride the rides.  
  
I’ll come back with you and I’ll tell you where we’ll go  
Hold my hand, hold tight, and we’ll run and stand in line for the  
big places shining on a seashore summer night  
We’ll dance, look at all the boys, and hope we’re pretty  
And stay that way, Then rush back home, drop the needle on the album.  
Grace Slicks’ voice will peel out like a saddened cry  
And you’llI want to hear her say it over and over  
  
*“Have you seen the stars tonight, take a look at, all,all...all the stars​”*  
  
The chords, they break my heart, and reach into something sadder  
but tomorrow I’ll try them out on the piano  
Jump out of bed in the morning and let the chords  
Fall onto the keys, the simple  
Progression calms the clutter in my head  
But tonight, I just want to sit on the Widow's Walk,  
It’s like a little boat in the nighttime sky  
That little porch up high, way up high, feel the wind  
and look out on a miniature world below,  
the lighted houses, low and summery cottages,  
look like little diamonds in the night  
We’ll talk and laugh about all the things we saw today.  
And play the “Yes” album all the way through,  
Let’s just be a little drunk and never get old,  
and let’s have another round.  
