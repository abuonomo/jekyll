---
layout: post
title:  "Smoke Signals"
date:   2020-01-08
categories: poems
---
I run through a countrified neighborhood I’m not used to  
drawn to a man burning leaves, sweet anachronistic smell  
pulls  me back to a long ago time -  oldest daughter,  
with a little sister and five older brothers round a dining room table,  
A small black and white television, junkyard bones now, but so modern for its time  
brought into the dining room at times of special disasters,  
assassinations and astronauts  
my mother reads a letter from my oldest brother in Vietnam  
Her voice gets shaky, bottom lip quivering, and my  
brother next in line takes over, the paper onion skin thin,  
He works on a ship, a floating hospital!  
He sails in far away places  -  Subic Bay and the South China Sea  
  
And sometimes in that year, he’d call from that ship, my mother gathering up strength  
reattaching herself to my brother with the big black cord,  
“Bobby’s on the phone!”  
We all run, drop everything, and line up age order for turns  
I know its my brother’s voice but you heard the distance back then  
It’s crackled with spaces in between that sound like a hundred waves crashing  
Or the cheers of a baseball game, and my mother and father  
hang on his every word masking worry with feigned cheeriness,  
But for my brothers, there’s a spark of adventure in it.  
using words like “over and out” their voices are loud and truncated  
I’m scared and rehearsing while they recount half-built go karts,  
fixing sports cars, baseball games, jobs and pretty girlfriends  
I can’t compete with any of it…  
and pressing shoulders behind me - my little sister  
who has the fearlessness and unbridled cuteness -  
that lovely lack of self awareness that God grants us for a little while.  
I’m nervous for my turn, even at ten years old there is much to be afraid of  
More than an ocean divides my brother and me .  
He’s watching bodies, young men that were in the bright beginnings of their lives  
with girlfriends back home  
dropping out of helicopters on stretchers, like damaged birds falling out of the sky slow motion,  
bandaged and bloody, missing arms and legs,  
While I’m still jumping rope, passing notes in the third grade  
And pretending to be married to a Beatle  
“I came out first in the spelling bee,” I tell him,  
  
Later that night my Father’s in the yard, pulling matches out of his pocket  
piles of leaves in the yard, the one’s my sister and I hid in that afternoon  
He’s tossing  the match, cool flick of his wrist,  
Over and over, the crispy burning sound, I’m watching the smoke  
ringing upward in swirls loops and s’s into the sky like a secret language  
the leaves wrinkling up upon themselves turning ashen  
throwing another match and another  
Thinking it normal, this man, my father,  enveloped in  
A grey fog in his backyard on an autumn night,  
In  his own private war, sending out smoke signals for help  
His oldest son nine thousand miles away in a war zone and  
an arrogant Country hellbent on itself to lead him there.  
  
My grandmother’s in the breakfast room, her small breathy body  
on a  stepstool rifling through the pine cabinet, her fingers  
gripping the forged iron handles, she’s all business tonight  
placing on the counter, spray bottles, starches, thimbles and thread  
My gloomy grandmother has the look of industry about her!  
There’s a project ahead and hopefulness in her eyes.  
She heard my brothers say in their booming voices  
he’d  be home for Thanksgiving!  
How she’ll iron and starch his Navy Dress Blues and Whites  
by the hour, remembering her own Navy man, making the regulation creases just right,  
And I believe they could stand up on their own without him in them  
  
My sister and I dash upstairs, skipping as many steps at a time to our room  
We perch at the windowsill on our elbows, peering out,  
the smoke has cleared and my father’s crossing the yard jangling coins in his pocket,  
his neck craning back,  and turning seeing us in the window  
“Girls, look up!”  Canadian Geese crossing way up high in a majestic V,  
Their distant caws and wings flapping unison  
a hundred heartbeats against the darkening sky.  
While back inside my brothers return to the lives that boys lead,  
Clunky collections of boy feet bounding upstairs, doors slamming,  
gathering in someone's room, gales of  laughter, one up-manship  
  
Meanwhile the elders are thinking larger thoughts  
My mother’s placing the letter under glass at her nightstand,  
Circling the date on her calendar  
While my sister and I will dive to the floor, like young girls do,  
We’ll  pull out from under our beds coloring books and crayons,  
John, Paul, George, and Ringo on the cover  
“ See, this is what we’ll do,” I’ll  say  
With an older sister protection in my voice  
“color their hair really dark, mix up the brown with the jet black  
their suits could be Indigo Blue, that’s the special blue  
I’m not scared anymore and the big house is turning on its axis,  
turning in a way that I  can understand.  
“ don’t leave any empty spaces, I’ll tell her,  
take your time and stay inside the lines  
Just remember to stay inside the lines.”  
